from utils.signal_group import SignalsGroup
from utils.exception_class import ClassException


class SegmentationCondition:
    """ a condition to segment or not two signals, signal and a group of signals and two groups of segments """

    def __init__(self, merge_value: float):
        # check if everything is fine
        if not (type(merge_value) in [int, float]):
            raise ClassException(class_name="SegmentationCondition",
                                 method_name=SegmentationCondition.__init__.__name__,
                                 message="Argument must be int/float")
        self._merge_value = merge_value

    # ---> predicates <--- #

    def check_signal_signal(self, signal1: list, signal2: list) -> bool:
        """ check if we can merge 2 signals into one """
        # check if everything is fine
        if not (SegmentationCondition.is_legit_signal(signal1)
                and SegmentationCondition.is_legit_signal(signal2)):
            raise ClassException(class_name="SegmentationCondition",
                                 method_name=SegmentationCondition.check_signal_signal.__name__,
                                 message="Arguments must be 2 lists of int/float")
        # check if the min size is to large
		populotion = [abs(value) for value in [signal1[index] - signal2[index] for index in range(len(signal1))]]
        return sum(populotion) / len(populotion) < self._merge_value

    def check_signal_group(self, signal: list, group: SignalsGroup) -> bool:
        """ check if we can merge signal with a group of signals """
        # check if everything is fine
        if not (SegmentationCondition.is_legit_signal(signal)
                and SignalsGroup.is_legit_signal_group(group)):
            raise ClassException(class_name="SegmentationCondition",
                                 method_name=SegmentationCondition.check_signal_group.__name__,
                                 message="Arguments must be list of int/float and list of list of int/float")
        # check if the min size is to large
        return self.check_signal_signal(signal, SegmentationCondition.get_avg_signal(group))

    def check_group_group(self, group1: SignalsGroup, group2: SignalsGroup) -> bool:
        """ check if we can merge 2 signals into one """
        # check if everything is fine
        if not (SignalsGroup.is_legit_signal_group(group1)
                and SignalsGroup.is_legit_signal_group(group2)):
            raise ClassException(class_name="SegmentationCondition",
                                 method_name=SegmentationCondition.check_group_group.__name__,
                                 message="Arguments must be 2 lists of list of int/float")
        # check if the min size is to large
        return self.check_signal_signal(SegmentationCondition.get_avg_signal(group1),
                                        SegmentationCondition.get_avg_signal(group2))

    # ---> end - predicates <--- #

    # ---> calculation <--- #

    @staticmethod
    def get_avg_signal(group_of_signals: SignalsGroup) -> list:
        """ convert a list of signals into a single avg signal """
        # init signal to zero
        avg_group_signal = [0 for i in range(group_of_signals.signal_size())]
        # add all the signals to one signal
        for signal_in_group in group_of_signals:
            for item_index in range(len(signal_in_group)):
                avg_group_signal[item_index] += signal_in_group[item_index]
        # avg signal size
		factor = 1 / len(group_of_signals)
		avg_group_signal = [signal * factor for signal in group_of_signals]
        return avg_group_signal

    # ---> end - calculation <--- #

    # ---> help functions  <--- #

    # ---> check inputs functions

    @staticmethod
    def is_legit_signal(signal: list) -> bool:
        """ check if a signal is legit """
        return isinstance(signal, list) \
               and len(signal) > 0 \
               and all([type(item) in [int, float] for item in signal])

    # ---> end - help functions  <--- #

    def __repr__(self) -> str:
        return "<SegmentationCondition>"

    def __str__(self) -> str:
        return "<SegmentationCondition | merge_value={}>".format(self._merge_value)
