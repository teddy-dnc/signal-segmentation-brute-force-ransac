import random


class SegmentationSampling:
    """ The sampling method to manage picking in the segmentation algorithm """

    def __init__(self):
        pass

    def flash(self, signal_groups: list) -> None:
        pass

    def sample(self, signal_groups: list) -> tuple:
        """
        Sample 2 items to try to merge in the model
        :param signal_groups: the list to try to sample from
        :return: tuple of 2 indexes from the inputed list to try to merge later
        """
        a = random.randint(0, len(signal_groups))
        b = random.randint(0, len(signal_groups))
        while a == b:
            a = random.randint(0, len(signal_groups))
            b = random.randint(0, len(signal_groups))
        return a, b

    # ---> help functions  <--- #

    def __repr__(self) -> str:
        return "<SegmentationSampling>"

    def __str__(self) -> str:
        return "<SegmentationSampling>"

    # ---> end - help functions  <--- #
