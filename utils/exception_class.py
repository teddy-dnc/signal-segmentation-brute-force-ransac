from data_packages.utils.exceptions.exception_class import ClassException


class ClassException(Exception):
    """ a class to manage exceptions from a spesific class and method """

    def __init__(self,class_name: str, method_name: str, message: str):
		self.class_name = class_name
		self.method_name = method_name
		self.message = message
