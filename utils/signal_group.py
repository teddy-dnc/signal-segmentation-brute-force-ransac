from utils.exception_class import ClassException


class SignalsGroup:
    """ a class to manage signals group """

    def __init__(self, signals: list, names: list):
        if not (SignalsGroup.is_legit_signal_group(signals) and not isinstance(signals, SignalsGroup)):
            raise ClassException(class_name="SignalsGroup",
                                 method_name=SignalsGroup.__init__.__name__,
                                 message="Arguments must be list of list of int/float")
        self._signals = signals
        self._signal_names = names

    def get(self, index: int) -> list:
        """ get the index item in the group """
        if 0 <= index < len(self._signals):
            return self._signals[index]
        raise OverflowError("index out of bound")

    def name_in_group(self, name: str) -> bool:
        """ check if signal by it's name is in this group """
        return name in self._signal_names

    def get_names(self) -> list:
        return self._signal_names

    def get_signals(self) -> list:
        return self._signals

    def signal_size(self) -> int:
        """ get the size of the signals in the group """
        return len(self._signals[0])

    def __iter__(self):
        """ returns the self object to be accessed by the for loop """
        return iter(self._signals)

    def __len__(self):
        return len(self._signals)

    def __repr__(self) -> str:
        return "<SignalsGroup | size={}>".format(len(self._signals))

    def __str__(self) -> str:
        return "<SignalsGroup | signals={}>".format(self._signal_names)

    @staticmethod
    def merge(first_group, second_group):
        """ merge into a single group of signals groups """
        signals = first_group._signals.copy()
        signals.extend(second_group._signals)
        names = first_group._signal_names.copy()
        names.extend(second_group._signal_names)
        return SignalsGroup(signals=signals,
                            names=names)

    @staticmethod
    def is_legit_signal_group(signal_group) -> bool:
        """ check if a group of signals is legit """
        return isinstance(signal_group, SignalsGroup) \
               or (isinstance(signal_group, list)
               and len(signal_group) > 0
               and ([isinstance(signal_in_group, list) for signal_in_group in signal_group])
               and all([all([type(item) in [int, float] for item in signal_in_group]) for signal_in_group in signal_group]))
