import SignalsGroup
import ClassException
import SegmentationCondition


class SegmentationStop:
    """ a condition to stop the segmentation process """

    def __init__(self):
        pass

    # ---> predicates <--- #

    @staticmethod
    def check_stop(groups: list, merge_condition: SegmentationCondition) -> tuple:
        """ check if we can merge 2 signals into one """
        if not all([SignalsGroup.is_legit_signal_group(group) for group in groups]):
            raise ClassException(class_name="SegmentationStop",
                                 method_name=SegmentationStop.check_stop.__name__,
                                 message="Arguments must be list of list of list of int/float")
        # check if can merge between all the groups into
        merged_list = [False for i in range(len(groups))]
        new_groups = []
        for groud_index_1 in range(len(groups)):
            merged = False
            if merged_list[groud_index_1]:
                continue
            for groud_index_2 in range(groud_index_1 + 1, len(groups)):
                if not merged_list[groud_index_2] \
                    and not merged_list[groud_index_2] \
                        and merge_condition.check_group_group(group1=groups[groud_index_1],
                                                              group2=groups[groud_index_2]):
                    new_groups.append(SignalsGroup.merge(first_group=groups[groud_index_1],
                                                         second_group=groups[groud_index_2]))
                    merged = True
                    merged_list[groud_index_1] = True
                    merged_list[groud_index_2] = True
                    break
            # if cannot merge remember as personal group
            if not merged and not merged_list[groud_index_1]:
                new_groups.append(groups[groud_index_1])
                merged_list[groud_index_1] = True
        # check if we can stop
        return (True, new_groups) if len(new_groups) == len(groups) else (False, new_groups)

    # ---> end - predicates <--- #

    # ---> calculation <--- #

    # ---> end - calculation <--- #

    # ---> help functions  <--- #

    def __repr__(self) -> str:
        return "<SegmentationStop>"

    def __str__(self) -> str:
        return "<SegmentationStop>"

    # ---> end - help functions  <--- #
