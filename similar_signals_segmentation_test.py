import math
import unittest
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import SegmentationSampling, SegmentationCondition, SegmentationStop, SignalsSegmentation


class SignalsSegmentationTest(unittest.TestCase):
    """
    A unit test class to check the performance of the SignalsSegmentation algorithm
    """

    def test_basic_merge_case(self):
        """ default sampling, merge and stop components for the merging """
        # input data
        signals = [[1, 1],
                   [0, 1],
                   [1, 0],
                   [0, 0],

                   [5, 5],
                   [5, 4],
                   [4, 5],
                   [4, 4],

                   [15, 15],
                   [14, 15],
                   [14, 14],
                   [16, 15],
                   [16, 16],
                   [15, 14],
                   [15, 16],
                   [16, 16],
                   [14, 16],
                   [16, 14],

                   [30, 30],
                   [31, 31],
                   [32, 32],
                   [33, 33],
                   [34, 34],
                   [35, 35],
                   [36, 36],

                   [20, 8],

                   [8, 20],

                   [22, 22]]
        right_number_of_segments = 7
        # try to merge
        merged = SignalsSegmentation(signals=signals).segment(segmentation_condition=SegmentationCondition(merge_value=4),
                                                              stop_interation=40,
                                                              stop_condition=SegmentationStop(),
                                                              sampling=SegmentationSampling(),
                                                              inner_step_size=5)
        # if we wish to present the answer as segments
        if True:
            x = [signal[0] for signal in signals]
            y = [signal[1] for signal in signals]
            plt.scatter(x, y, color='g' , alpha=0.5, marker="o")
            centers = [SegmentationCondition.get_avg_signal(group) for group in merged]
            c_x = [center[0] for center in centers]
            c_y = [center[1] for center in centers]
            plt.scatter(c_x, c_y, color='r', alpha=0.9, marker="^")
            plt.show()

        # if the right number of segments we good enough
        self.assertEqual(len(merged), right_number_of_segments)

    def test_basic_merge_case_bias_between_groups(self):
        """ default sampling, merge and stop components for the merging """
        # input data
        signals = [[1, 1],
                   [0, 1],
                   [1, 0],
                   [0, 0],

                   [4, 4],
                   [4, 3],
                   [3, 4],
                   [3, 3],

                   [30, 30],
                   [31, 31],
                   [32, 32],
                   [33, 33],
                   [34, 34],
                   [35, 35],
                   [36, 36],

                   [30, 33],
                   [31, 34],
                   [32, 35],
                   [33, 36],
                   [34, 37],
                   [35, 38],
                   [36, 39]]
        # add circle
        pi = math.pi
        r = 5
        n = 60
        x_shift = 15
        y_shift = 15
        signals.extend([[x_shift + math.cos(2 * pi / n * x) * r,
                         y_shift + math.sin(2 * pi / n * x) * r]
                        for x in range(0, n + 1)])

        right_number_of_segments = 3
        # try to merge
        merged = SignalsSegmentation(signals=signals).segment(segmentation_condition=SegmentationCondition(merge_value=5),
                                                              stop_interation=40,
                                                              stop_condition=SegmentationStop(),
                                                              sampling=SegmentationSampling(),
                                                              inner_step_size=5)
        # if we wish to present the answer as segments
        if True:
            x = [signal[0] for signal in signals]
            y = [signal[1] for signal in signals]
            plt.scatter(x, y, color='g' , alpha=0.5, marker="o")
            centers = [SegmentationCondition.get_avg_signal(group) for group in merged]
            c_x = [center[0] for center in centers]
            c_y = [center[1] for center in centers]
            plt.scatter(c_x, c_y, color='r', alpha=0.9, marker="^")
            plt.show()

        # if the right number of segments we good enough
        self.assertEqual(right_number_of_segments, len(merged))

        right_number_of_segments = 7
        # try to merge
        merged = SignalsSegmentation(signals=signals).segment(segmentation_condition=SegmentationCondition(merge_value=3),
                                                              stop_interation=40,
                                                              stop_condition=SegmentationStop(),
                                                              sampling=SegmentationSampling(),
                                                              inner_step_size=5)
        # if we wish to present the answer as segments
        if True:
            x = [signal[0] for signal in signals]
            y = [signal[1] for signal in signals]
            plt.scatter(x, y, color='g' , alpha=0.5, marker="o")
            centers = [SegmentationCondition.get_avg_signal(group) for group in merged]
            c_x = [center[0] for center in centers]
            c_y = [center[1] for center in centers]
            plt.scatter(c_x, c_y, color='r', alpha=0.9, marker="^")
            plt.show()

        # if the right number of segments we good enough
        self.assertTrue(right_number_of_segments <= len(merged))

    def test_three_dim_merge(self):
        """ default sampling, merge and stop components for the merging """
        # input data
        signals = []
        # add full box
        n = 3
        x_shift = 4
        y_shift = 4
        z_shift = 4
        for x in range(x_shift, x_shift + n):
            for y in range(y_shift, y_shift + n):
                for z in range(z_shift, z_shift + n):
                    signals.append([x, y, z])

        ndim = 120
        vec = np.random.randn(ndim, n)
        vec /= np.linalg.norm(vec, axis=0)
        signals.extend(vec.tolist())

        right_number_of_segments = 2
        # try to merge
        merged = SignalsSegmentation(signals=signals).segment(segmentation_condition=SegmentationCondition(merge_value=3),
                                                              stop_interation=40,
                                                              stop_condition=SegmentationStop(),
                                                              sampling=SegmentationSampling(),
                                                              inner_step_size=5)
        # if we wish to present the answer as segments
        if True:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            x = [signal[0] for signal in signals]
            y = [signal[1] for signal in signals]
            z = [signal[2] for signal in signals]
            ax.scatter(x, y, z, color='g', alpha=0.5,  marker="o")
            centers = [SegmentationCondition.get_avg_signal(group) for group in merged]
            c_x = [center[0] for center in centers]
            c_y = [center[1] for center in centers]
            c_z = [center[2] for center in centers]
            ax.scatter(c_x, c_y, c_z, color='r', alpha=0.9, marker="^")
            ax.set_xlabel('X Label')
            ax.set_ylabel('Y Label')
            ax.set_zlabel('Z Label')
            plt.show()

        # if the right number of segments we good enough
        self.assertEqual(right_number_of_segments, len(merged))


if __name__ == '__main__':
    SignalsSegmentationTest().test_basic_merge_case()
    SignalsSegmentationTest().test_basic_merge_case_bias_between_groups()
    SignalsSegmentationTest().test_three_dim_merge()
