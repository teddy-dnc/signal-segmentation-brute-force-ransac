import SegmentationCondition
from utils.exception_class import ClassException


class SegmentationConditionDiversion(SegmentationCondition):
    """
        a condition to segment or not two signals, signal and a group of signals and two groups of segments .
        Taking advantage on the upper and donwer local process of the signal
    """

    def __init__(self, merge_value: float):
        # check if everything is fine
        if not (type(merge_value) in [int, float] and 0 < merge_value <= 1):
            raise ClassException(class_name="SegmentationCondition",
                                 method_name=SegmentationConditionDiversion.__init__.__name__,
                                 message="Argument must be int/float between 0 and 1")
        SegmentationCondition.__init__(self, merge_value=merge_value)

    # ---> predicates <--- #

    def check_signal_signal(self, signal1: list, signal2: list) -> bool:
        """ check if we can merge 2 signals into one """
        # check if everything is fine
        if not (SegmentationCondition.is_legit_signal(signal1)
                and SegmentationCondition.is_legit_signal(signal2)):
            raise ClassException(class_name="SegmentationConditionDiversion",
                                 method_name=SegmentationConditionDiversion.check_signal_signal.__name__,
                                 message="Arguments must be 2 lists of int/float")
        # check if the min size is to large
        diversion_signal1 = SegmentationConditionDiversion.diversion_signal(signal1)
        diversion_signal2 = SegmentationConditionDiversion.diversion_signal(signal2)
        counter = 0
        for i in range(len(diversion_signal1)):
            if (diversion_signal1[i] > 0 and diversion_signal2[i] > 0) or \
                (diversion_signal1[i] < 0 and diversion_signal2[i] < 0) or \
                    (diversion_signal1[i] == 0 and diversion_signal2[i] == 0):
                counter += 1
        return counter / len(diversion_signal1) >= self._merge_value

    # ---> end - predicates <--- #

    # ---> calculation <--- #

    @staticmethod
    def diversion_signal(signal: list) -> list:
        """ convert a signal into the front diversion single of the same signal"""
        return [signal[i+1] - signal[i] for i in range(len(signal)-1)]

    # ---> end - calculation <--- #

    # ---> help functions  <--- #

    # ---> check inputs functions

    # ---> end - help functions  <--- #

    def __repr__(self) -> str:
        return "<SegmentationConditionDiversion>"

    def __str__(self) -> str:
        return "<SegmentationConditionDiversion | merge_value={}>".format(self._merge_value)
