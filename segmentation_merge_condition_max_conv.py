import Statistical
import ClassException
import VectorsOperations
import SegmentationCondition


class SegmentationConditionMaxConv(SegmentationCondition):
    """
        a condition to segment or not two signals, signal and a group of signals and two groups of segments .
        Taking advantage on the upper and donwer local process of the signal
    """

    def __init__(self, merge_value: float):
        SegmentationCondition.__init__(self, merge_value=merge_value)

    # ---> predicates <--- #

    def check_signal_signal(self, signal1: list, signal2: list) -> bool:
        """ check if we can merge 2 signals into one """
        # check if everything is fine
        if not (SegmentationCondition.is_legit_signal(signal1)
                and SegmentationCondition.is_legit_signal(signal2)):
            raise ClassException(class_name="SegmentationConditionMaxConv",
                                 method_name=SegmentationConditionMaxConv.check_signal_signal.__name__,
                                 message="Arguments must be 2 lists of int/float")
        # check if the min size is to large
        best_conv = SegmentationConditionMaxConv.conv_signals_case(signal1=signal1,
                                                                   signal2=signal2)
        for move_index in range(1, len(signal1)-1):
            current_conv = SegmentationConditionMaxConv.conv_signals_case(signal1=SegmentationConditionMaxConv.move_signal(signal=signal1,
                                                                                                                           move=move_index),
                                                                          signal2=signal2)
            if current_conv < best_conv:
                best_conv = current_conv
        return best_conv < self._merge_value

    # ---> end - predicates <--- #

    # ---> calculation <--- #

    @staticmethod
    def move_signal(signal: list, move: int) -> list:
        """ move the signal by 'move' places ahead """
        return [signal[(i + move) % len(signal)] for i in range(len(signal))]

    @staticmethod
    def conv_signals_case(signal1: list, signal2: list) -> float:
        """ run the discredit 1d conv of the 2 signals """
        return Statistical.mean(population=VectorsOperations.abs(vector=VectorsOperations.sub(p1=signal1, p2=signal2)))

    # ---> end - calculation <--- #

    # ---> help functions  <--- #

    # ---> check inputs functions

    # ---> end - help functions  <--- #

    def __repr__(self) -> str:
        return "<SegmentationConditionDiversion>"

    def __str__(self) -> str:
        return "<SegmentationConditionDiversion | merge_value={}>".format(self._merge_value)
