import math

import SegmentationStop
import SegmentationSampling
import SegmentationCondition
import SegmentationConditionMaxConv
import SegmentationConditionDiversion
from utils.signal_group import SignalsGroup
from utils.exception_class import ClassException


class SignalsSegmentation:
    """ segment a list of singles using a similarity function """

    def __init__(self, signals: list, names: list):
        if not (isinstance(signals, list) and len(signals) >= 2
                and all([isinstance(signal, list) for signal in signals])
                and isinstance(names, list) and len(names) >= 2
                and all([isinstance(name, str) for name in names])
                and len(names) == len(signals)
                and all([all([type(item) in [int, float] for item in signal]) for signal in signals])):
            raise ClassException(class_name="SignalsSegmentation",
                                 method_name=SignalsSegmentation.__init__.__name__,
                                 message="Arguments must be list of list of int/float")
        self._singles = signals
        self._names = names

    def segment(self,
                segmentation_condition: SegmentationCondition,
                stop_interation: int,
                stop_condition: SegmentationStop,
                sampling: SegmentationSampling,
                inner_step_size: int) -> list:
        """ run the segmentation algorithm with merge condition, source signals,
            stop interaction to prevent infinite loop and stop_condition to get out faster """
        # init meta-data for the algorithm running
        iteration_number = 0
        signal_groups = [SignalsGroup(signals=[signal], names=[self._names[index]]) for index, signal in enumerate(self._singles)]
        stop_condition_fulfill = False
        # run until we pass some pre-defended number of iterations or the stop condition is fulfilled
        while iteration_number < stop_interation and not stop_condition_fulfill:
            # delete sampling condition's inner memory for new kind of sampling if needed
            sampling.flash(signal_groups)
            # do few merge steps between stop condition checks cause it is expensive
            for inner_step in range(round(min(inner_step_size, math.ceil(len(signal_groups)/2)))):
                try:
                    # find what samples we wish to try and merge
                    try_merge_index1, try_merge_index2 = sampling.sample(signal_groups)
                    # check if we can merge
                    if segmentation_condition.check_group_group(group1=signal_groups[try_merge_index1],
                                                                group2=signal_groups[try_merge_index2]):
                        # merge into one list
                        signal_groups.append(SignalsGroup.merge(first_group=signal_groups[try_merge_index1],
                                                                second_group=signal_groups[try_merge_index2]))
                        # delete the two original groups
                        signal_groups.remove(signal_groups[try_merge_index1])
                        signal_groups.remove(signal_groups[try_merge_index2])
                    iteration_number += 1
                except Exception as error:
                    print("Error at it = {} saying: {}".format(iteration_number, error))
            # check if we can stop now and run heavy merging process
            stop_condition_fulfill, signal_groups = stop_condition.check_stop(groups=signal_groups,
                                                                              merge_condition=segmentation_condition)
        # return the answers
        return signal_groups

    def __repr__(self) -> str:
        return "<SignalsSegmentation>"

    def __str__(self) -> str:
        return "<SignalsSegmentation | len(singles)={}>".format(self._singles)
